#!/bin/bash

nrep=4
T1=300.0
T2=300.0

for((i=0;i<nrep;i++)) ; do 
 ii=$(($i+1))
 temp=`echo " e( ( ${i} / ( ${nrep} - 1 ) ) * l( $T2 / $T1 )) * $T1 " | bc -l | awk '{printf "%6.3f",$1}' `
 echo "temperature: $temp"
 sleep 2
 sed -e "s/TEMP_/${temp}/g" TEMPLATE_md.mdp > md.mdp 
 echo "********************************************"
 echo "  "
 gmx_2016_sp_serial editconf -f conf${i}.gro -o conf${i}_box.gro -bt cubic -d 0.0 -box 3 -c
 gmx_2016_sp_serial grompp -f md.mdp -c conf${i}_box.gro -p topol.top -maxwarn 10 -o topol${i}.tpr 
 echo "  "
 echo "********************************************"
 rm md*.mdp
done

rm \#*

