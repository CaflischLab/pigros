#!/bin/bash
diff -y traj2.pdb traj0.pdb > t2t0.diff
grep -v "|" t2t0.diff > t2t0.same         #I expect very little here to be similar
diff -y traj2.pdb traj1.pdb > t2t1.diff
grep -v "|" t2t1.diff > t2t1.same
diff -y traj2.pdb traj3.pdb > t2t3.diff
grep -v "|" t2t3.diff > t2t3.same
diff -y traj1.pdb traj3.pdb > t1t3.diff
grep -v "|" t1t3.diff > t1t3.same
