26/04/18: First full reseeding cycly took place!
Everything seems fine to me.
Now, what remains:

(1) Correct for periodicity and for refmol (I need pdb-mol and refmol in gromacs) -> look in do_force for functions that correct distance vectors and to move molecules around. 
(2) Implement the other distances (RMSD and TORs)
(3) Tackle parallelization.

Forseen end: End of August.


