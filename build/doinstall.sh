#!/bin/bash
#
instllsffx=sp_mpi #for installation directory
where=`pwd`
##CMAKE_C_FLAGS="-g"
##CMAKE_CCX_FLAGS="-g"
#
#touch ../src/programs/mdrun/md.cpp ; touch ../src/programs/mdrun/repl_ex.cpp ; 
#cmake ../ -DCMAKE_BUILD_TYPE=Release -DGMX_BUILD_OWN_FFTW=ON -DREGRESSIONTEST_DOWNLOAD=OFF -DCMAKE_INSTALL_PREFIX=${where}/${instllsffx} -DGMX_GPU=OFF -DGMX_USE_OPENCL=OFF -DGMX_MPI=ON -DGMX_THREAD_MPI=OFF -DGMX_DEFAULT_SUFFIX=OFF -DGMX_BINARY_SUFFIX=_${instllsffx} -DGMX_LIBS_SUFFIX=_${instllsffx} >& log_cmake_${instllsffx}.log 
#
make -j4 VERBOSE=1;
#echo "make check >& log_make_check_${instllsffx}.log"
#make check >& log_make_check_${instllsffx}.log
#echo "done"
#
make install #>& log_make_install_${instllsffx}.log
sleep 2
mv install_manifest.txt install_manifest_${instllsffx}.txt
#
#to remove gromacs:
#
#xargs rm < install_manifest.txt
#
