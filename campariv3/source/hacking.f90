!--------------------------------------------------------------------------!
! LICENSE INFO:                                                            !
!--------------------------------------------------------------------------!
!    This file is part of CAMPARI.                                         !
!                                                                          !
!    Version 3.0                                                           !
!                                                                          !
!    Copyright (C) 2017, The CAMPARI development team (current and former  !
!                        contributors)                                     !
!                        Andreas Vitalis, Adam Steffen, Rohit Pappu, Hoang !
!                        Tran, Albert Mao, Xiaoling Wang, Jose Pulido,     !
!                        Nicholas Lyle, Nicolas Bloechliger, Marco Bacci,  !
!                        Davide Garolini, Jiri Vymetal                     !
!                                                                          !
!    Website: http://sourceforge.net/projects/campari/                     !
!                                                                          !
!    CAMPARI is free software: you can redistribute it and/or modify       !
!    it under the terms of the GNU General Public License as published by  !
!    the Free Software Foundation, either version 3 of the License, or     !
!    (at your option) any later version.                                   !
!                                                                          !
!    CAMPARI is distributed in the hope that it will be useful,            !
!    but WITHOUT ANY WARRANTY; without even the implied warranty of        !
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         !
!    GNU General Public License for more details.                          !
!                                                                          !
!    You should have received a copy of the GNU General Public License     !
!    along with CAMPARI.  If not, see <http://www.gnu.org/licenses/>.      !
!--------------------------------------------------------------------------!
! AUTHORSHIP INFO:                                                         !
!--------------------------------------------------------------------------!
!                                                                          !
! MAIN AUTHOR:   Andreas Vitalis                                           !
!                                                                          !
!--------------------------------------------------------------------------!
!
!--------------------------------------------------------------------------
!
! use this file to speed up incremental compiles by putting the functions/subroutines under 
! development here
! once debugging and testing is complete, you can move them to the target source-file
!
! do not remove "i_am_a_bad_hack" and do not keep functions/subroutines permanently in here
!
#include"macros.i"
!
!--------------------------------------------------------------------------
!
subroutine i_am_a_bad_hack(tpi)
!
  integer, INTENT(IN):: tpi
!
end
!
!-------------------------------------------------------------------------
!
!-------------------------------------------------------------------------
!
subroutine pigros_dokeyfl(tmp_recond,klen,instr) BIND(C) !supposedly doing all the initializations and checks
!
  use, INTRINSIC :: ISO_C_BINDING
!
  use iounit
  use clusters !, ONLY: cfilen,cdis_crit,cstorecalc,cmaxsnaps,cludata,cdofset,cstored,sconnect,sconnectsz,&
               !       &cmode,cprogindex,cprogindstart,cprogfold,ccfepmode,cequil,caddlkmd,&
               !       &calcsz,& !this is the number of features, including dyn weights, which I disable for now
               !       &clstsz,&   !this is the number of features, without dyn weights
               !       &pigros_catms,pigros_ncatms,pigros_ofileu,pigros_catmmap,pigros_cdofset,pigros_ispigros
  use commline
  use interfaces
  use mpistuff, ONLY: re_freq,re_aux,re_conditions,inst_retr
  use system, ONLY: nsim,nequil,pdb_analyze
!
  implicit none
!
  integer(C_INT), INTENT(IN):: tmp_recond
  integer(C_SIZE_T), INTENT(IN), VALUE :: klen
  character(kind=c_char), dimension(*), intent(IN) :: instr
  integer :: slen = 0
  character(LEN=klen) kfl
!  
!  character(klen), INTENT(IN):: kfl !this seems to work without BIND(C)
!  character(47) :: kfl = "/home/mbacci/temp/pigs-grmcs/grmcs/key_pigs.key"
!
  character(LEN=18), PARAMETER :: filenm = "log_campa_pigs.log" !34 //CHAR(0)
  integer i,j,k,ii
  integer freeunit,t1,t2,nlst
  integer, ALLOCATABLE:: tmpveco(:), tmpveci(:)
  logical exists,lsort,notdone
!
  lsort = .true.
  inst_retr = .true.
!
! It should not be allowed to put a person in the situation of having to deal with the things below (till getkey included)
  write(*,*) "Start of pigros_dokeyfl"
  do while (.true.)
    if (instr(slen+1) == C_NULL_CHAR) exit
    slen = slen + 1
  end do
  write(*,*) 'CAMPARI: Lenth of string for keyfile is: ',slen
  write(*,*) 'CAMPARI: The received length is: ',klen
  if (slen.ne.klen) then
    write(*,*) 'CAMPARI FATAL. Lengths of strings for keyfile do not match. This is a bug.'
  end if 
  write(*,*) 'CAMPARI: The received instr is: ',instr(1:slen)
  kfl(1:1) = instr(1)
  do i=2,slen
    kfl(i:i) = instr(i)
  end do
!
  write(*,*) 'CAMPARI: tmp_recond',tmp_recond
  write(*,*) 'CAMPARI: keyfile ',kfl
  write(*,*) 'CAMPARI: Starting pigros_dokeyfl...see: ',filenm
!
  pigros_ofileu = freeunit()
  open(pigros_ofileu,file=filenm,status="unknown",action="write")
  write(pigros_ofileu,*) "------------------------------------"
  write(pigros_ofileu,*) "Start of pigros_dokeyfl"
!
  write(pigros_ofileu,*) 'calling initial()'
  pigros_ispigros = .true.
  call initial()
  write(pigros_ofileu,*) 'done with initial'
!
  if (allocated(args)) deallocate(args)
  nargs = 2
  allocate(args(1:nargs))
!
  args(1)%it = '-k'
  args(2)%it = kfl
  write(pigros_ofileu,*) 'calling getkey()'
  call getkey()
  call init_campprng(1)
  write(pigros_ofileu,*) 'done with getkey'
  write(pigros_ofileu,*)
  write(pigros_ofileu,*) '---   Now parsing keywords ...            ---'
  write(pigros_ofileu,*)
  call parsekey(1)
  call parsekey(2)
  call parsekey(3)
!
  call strlims(cfilen,t1,t2)
  inquire(file=cfilen(t1:t2),exist=exists)
  if (exists.EQV..false.) then
    write(*,*) 'CAMPARI FATAL. CFILE not found. It is mandatory to use one with PIGS in GROMACS. This may change in the future.'
    call fexit()
  end if
!
  write(pigros_ofileu,*)
  write(pigros_ofileu,*) '---   ... finished parsing keywords.      ---'
  write(pigros_ofileu,*)
! 
  write(pigros_ofileu,*)
  write(pigros_ofileu,*) '---   CAMPARI INITIALIZATIONS             ---'
  write(pigros_ofileu,*)
!
  call makepept()
!
! hardcoded stuff
  re_conditions = tmp_recond !passed by gromacs
  nequil = 0             
  pdb_analyze = .true.   !emulating pigs anal mode
  cmode = 4
  cprogindex = 2
  cprogindstart = -1
  cprogfold = 0
  ccfepmode = 0
  cequil = 0
  caddlkmd = 0
  nsim = re_freq         !length of a stretch
  re_aux(8) = nsim                             !re_conditions is number of replicas
  cstored = (re_freq/cstorecalc)*re_conditions !total number of snapshots stored in cludata
!
  if (mod(re_freq,cstorecalc).ne.0) then
    write(*,*) 'CAMPARI FATAL. Got REFREQ not an integer multiple of CCOLLECT.'
    call fexit()
  end if
!
  if (allocated(sconnect).EQV..true.) deallocate(sconnect) !build_sconnect basically
  sconnectsz = cstored 
  allocate(sconnect(sconnectsz,5))
  sconnect(:,:) = 0
  do i=1,cstored-1
    sconnect(i,1) = i
    if (mod(i,nsim)==0) then 
      write(pigros_ofileu,*) 'Breaking link at edge: ',i,i+1
    end if
    sconnect(i,2) = i+1
    sconnect(i,3) = i
    sconnect(i+1,5) = i
  end do
  sconnect(cstored,1) = cstored
!
  write(pigros_ofileu,*) 'This is the keyfile (received): ',kfl
  write(pigros_ofileu,*) 'This is numer of replicas (received): ',re_conditions
  write(pigros_ofileu,*) 'This is stretch length REFREQ: ',re_freq
  write(pigros_ofileu,*) 'This is CFILE: ',trim(cfilen)
  write(pigros_ofileu,*) 'This is CCCOLLECT: ',cstorecalc
  write(pigros_ofileu,*) 'This is number of snapshots in cludata: ',cstored
  write(pigros_ofileu,*) 'This is RE_TRAJTOTAL: ',re_aux(8)
  write(pigros_ofileu,*) 'calling read_clusteringfile...'
  flush(pigros_ofileu)
  call read_clusteringfile() !this allocate cludata afaik
  if (allocated(cludata).EQV..false.) then
    write(*,*) 'CAMPARI FATAL. Got cludata not allocated by read_clusteringfile. This is a bug afaik.'
    call fexit()
  end if
!
  if (cmaxsnaps.le.3) then
    write(*,*) 'CAMPARI FATAL. Not enough snapshots for clustering.'
    call fexit() 
  end if
  if ((calcsz.le.0).OR.(clstsz.le.0)) then
    write(*,*) 'CAMPARI FATAL. No features for clustering.'
    call fexit() 
  end if
  if ((cdis_crit.eq.2).OR.(cdis_crit.eq.4).OR.(cdis_crit.eq.6).OR.(cdis_crit.ge.8)) then 
    write(*,*) 'CAMPARI FATAL. No weights for clustering with PIGS in GROMACS, and no CDIST 6. This may change in the future.'
    call fexit() 
  end if 
  write(pigros_ofileu,*) 'This is CDISTANCE: ',cdis_crit
  write(pigros_ofileu,*) 'This is the number of features from CFILE: ',clstsz
  write(pigros_ofileu,*) 'This is the number of features and weigths from CFILE and CDISTANCE: ',calcsz
  write(pigros_ofileu,*) 'This are the sizes of cdofset: ',size(cdofset,dim=1),size(cdofset,dim=2)
  if ((size(cdofset,dim=1).ne.clstsz).OR.(size(cdofset,dim=2).ne.2)) then
    write(*,*) 'CAMPARI FATAL. (size(cdofset,dim=1).ne.clstsz).OR.(size(cdofset,dim=2).ne.2). This is a bug afaik.'
  end if
  do i=1,clstsz
    write(pigros_ofileu,*) 'cdofset(i,1),cdofset(i,2): ',cdofset(i,1),cdofset(i,2)
  end do
  if (allocated(pigros_cdofset).EQV..true.) deallocate(pigros_cdofset)
  if (allocated(pigros_catmmap).EQV..true.) deallocate(pigros_catmmap) !from pigros_cdofset to pigros_catms
!
  select case(cdis_crit)
    case(1:4)
      allocate(pigros_cdofset(clstsz,4)) !dihedrals require 4 atoms to define the feature 
      allocate(pigros_catmmap(clstsz,4))
      pigros_catmmap = 0
      write(*,*) 'CAMPARI FATAL. Dihedral distances still to be implemented.' !here I will have to cdof -> 4 dihedral atoms
      call fexit()
    case(5:6,10)
      allocate(pigros_cdofset(clstsz,1)) !rmsd requires only 1 atom to define the feature
      allocate(pigros_catmmap(clstsz,1))
      pigros_catmmap = 0
      do i=1,clstsz
        pigros_cdofset(i,1) = cdofset(i,1) !assuming there is nothing interesting in 2nd column of cdofset here, shall check
      end do
    case(7:9)
      allocate(pigros_cdofset(clstsz,2)) !interatomic distances require only 1 atom to define the feature
      allocate(pigros_catmmap(clstsz,2))
      pigros_catmmap = 0
      do i=1,clstsz
        pigros_cdofset(i,1) = cdofset(i,1) 
        pigros_cdofset(i,2) = cdofset(i,2) 
      end do
  end select
!
  j = 1
  select case(cdis_crit)
    case(1:4) !diheds
     nlst = 4*clstsz
     allocate(tmpveco(nlst))
     allocate(tmpveci(nlst))
     do i=1,clstsz
       tmpveco(i) = pigros_cdofset(i,1)
       tmpveci(i) = pigros_cdofset(i,1)
       tmpveco(i+clstsz) = pigros_cdofset(i,2)
       tmpveci(i+clstsz) = pigros_cdofset(i,2)
       tmpveco(i+(2*clstsz)) = pigros_cdofset(i,3)
       tmpveci(i+(2*clstsz)) = pigros_cdofset(i,3)
       tmpveco(i+(3*clstsz)) = pigros_cdofset(i,4)
       tmpveci(i+(3*clstsz)) = pigros_cdofset(i,4)
     end do
     call merge_sort(ldim=nlst,up=lsort,list=tmpveci,olist=tmpveco(1:nlst),ilo=j,ihi=nlst)
    case(5,6,10) !RMSDs
      nlst = clstsz
      allocate(tmpveco(nlst))
      tmpveco = pigros_cdofset(:,1)
      call merge_sort(ldim=nlst,up=lsort,list=pigros_cdofset(:,1),olist=tmpveco(1:nlst),ilo=j,ihi=nlst) 
    case (7,8,9) !interatomic distances
     nlst = 2*clstsz
     allocate(tmpveco(nlst))
     allocate(tmpveci(nlst))
     do i=1,clstsz
       tmpveco(i) = pigros_cdofset(i,1)
       tmpveci(i) = pigros_cdofset(i,1)
       tmpveco(i+clstsz) = pigros_cdofset(i,2)
       tmpveci(i+clstsz) = pigros_cdofset(i,2)
     end do
     call merge_sort(ldim=nlst,up=lsort,list=tmpveci,olist=tmpveco(1:nlst),ilo=j,ihi=nlst) 
  end select
!
  notdone = .true.
  i = 2
  do while (notdone.EQV..true.)
    if (tmpveco(i).eq.tmpveco(i-1)) then
      tmpveco((i-1):(nlst-1)) = tmpveco(i:nlst)
      nlst = nlst - 1
    else
      i = i + 1
    end if
    if (i.gt.nlst) notdone = .false.
  end do
!
  pigros_ncatms = nlst !how many non-duplicated atoms are in the representation
  if (allocated(pigros_catms).EQV..true.) deallocate(pigros_catms)
  allocate(pigros_catms(pigros_ncatms))
  do i=1,pigros_ncatms
    pigros_catms(i) = tmpveco(i)
    write(pigros_ofileu,*) 'Atoms to GROMACS:',pigros_catms(i) !this I have to read in gromacs
  end do
  if (allocated(tmpveci).EQV..true.) deallocate(tmpveci)
  if (allocated(tmpveco).EQV..true.) deallocate(tmpveco)
!
  do j=1,size(pigros_cdofset,dim=2)
    do i=1,clstsz 
      ii = pigros_cdofset(i,j)
      do k=1,nlst
        if (ii==pigros_catms(k)) then
          pigros_catmmap(i,j) = k
          exit 
        end if
      end do
      if (pigros_catms(pigros_catmmap(i,j)).ne.pigros_cdofset(i,j)) then
        write(*,*) "CAMPARI FATAL. Wrong map (1)."
        call fexit()
      end if
    end do
  end do
!
  write(pigros_ofileu,*) 'done calling read_clusteringfile'
!
  write(pigros_ofileu,*)
  write(pigros_ofileu,*) '---   DONE WITH CAMPARI INITIALIZATIONS   ---'
  write(pigros_ofileu,*)
  write(pigros_ofileu,*) 'Done pigros_dokeyfl'
  write(pigros_ofileu,*) "------------------------------------"
  flush(pigros_ofileu)
!
  write(*,*) 'Done pigros_dokeyfl'
!
end subroutine pigros_dokeyfl
!
!--------------------------------------------------------------------------
!
! copying to temporary cludata (final cludata will have to process temporary cludata for cdist, weights, ...)
subroutine pigros_receive_cludata(nrepls,nsnaps,nfeats,pigs_cludata_grmcs,istep) BIND(C)
!
  use, INTRINSIC :: ISO_C_BINDING
!
  use clusters, ONLY: pigros_ofileu
!
  implicit none
!
  integer(C_INT), INTENT(IN):: nrepls
  integer(C_INT), INTENT(IN):: nsnaps
  integer(C_INT), INTENT(IN):: nfeats
!  integer(KIND=8), INTENT(IN):: istep
  integer(C_INT64_T), INTENT(IN):: istep
!  real(KIND=4), INTENT(IN):: pigs_cludata_grmcs(nrepls*nsnaps*nfeats)
  real(C_FLOAT), INTENT(IN):: pigs_cludata_grmcs(nrepls*nsnaps*nfeats)
!
  integer counter,irep,isnap,ifeat
  real(KIND=4) pigs_cludata_campa(nfeats,nsnaps*nrepls)
!
  write(*,*) "Start of pigros_receive_cludata"
  write(pigros_ofileu,*) "------------------------------------"
  write(pigros_ofileu,*) "Start of pigros_receive_cludata"
  write(pigros_ofileu,*) "This is the dimension of linear cludata:",nrepls*nsnaps*nfeats
  counter = 0
  do irep=1,nrepls
    do isnap=1,nsnaps
      do ifeat=1,nfeats
        counter = counter + 1
        pigs_cludata_campa(ifeat,isnap+((irep-1)*nsnaps)) = pigs_cludata_grmcs(counter)
!        write(pigros_ofileu,'(A34,2X,4I9,2x,F9.3,2x,F9.3)') 'F CODE, irep, isnap, ifeat, data: ',irep,isnap,ifeat,&
! &                   counter,pigs_cludata_grmcs(counter),pigs_cludata_campa(ifeat,isnap+((irep-1)*nsnaps))
      end do
    end do
  end do
  write(pigros_ofileu,*) 'Done with pigros_receive_cludata'
  write(pigros_ofileu,*) "------------------------------------"
  flush(pigros_ofileu)
!
  call pigros_docludata(nfeats,nsnaps,nrepls,pigs_cludata_campa,istep) !manages everything about cludata
!
  write(*,*) 'Done with pigros_receive_cludata'
!
end subroutine pigros_receive_cludata
!
!-------------------------------------------------------------------------
!
! to set up cludata by using pigros_docludata: ONGOING, depending on what cludata from gromacs contains
subroutine pigros_docludata(nfeats,nsnaps,nrepls,pigs_cludata_campa,istep)

  use clusters, ONLY: cdis_crit,calcsz,clstsz,cludata,cdistrans_params,cdistransform,&
 &                    pigros_ofileu,pigros_catms,pigros_catmmap,pigros_cdofset,pigros_nmap
  use iounit
  use math
  use mcsums, ONLY: iretr
  use mpistuff, ONLY: myrank,mpi_nodes,re_conditions
!
  implicit none
  integer, INTENT(IN):: nrepls
  integer, INTENT(IN):: nsnaps
  integer, INTENT(IN):: nfeats
  integer(KIND=8), INTENT(IN):: istep
  logical :: isfirstcall = .true.  !this should be equivalent to static in C, I could also put save but should not matter afaik
  real(KIND=4), INTENT(IN):: pigs_cludata_campa(nfeats,nsnaps*nrepls)
!
  integer :: tpi = 0                        ! -> run threads_init and set up of parallel region as in chainsaw
  integer i,j,k,ii,jj,iii,freeunit
  RTYPE coor1(3),coor2(3),coor3(3),coor4(3) !to store xyz of atoms (max 4 xyz for a dihedral)
  RTYPE featval,tmpzt                       !value of the feature that goes into cludata 
  logical exists
  character(LEN=19), PARAMETER :: pitrfile = "N_000_PIGSTRACE.dat"
!
  write(*,*) "Start of pigros_docludata"
  write(pigros_ofileu,*) "------------------------------------"
  write(pigros_ofileu,*) "Start of pigros_docludata"
  iretr = freeunit()
  inquire(file=pitrfile,exist=exists)
  if (exists.EQV..false.) then
    write(pigros_ofileu,*) 'Initializing N_000_PIGSTRACE'
    open(iretr,file=pitrfile,status="new",action="write")
  else
    open(iretr,file=pitrfile,status="old",action="write",position="append")
    write(pigros_ofileu,*) 'Appending to existing N_000_PIGSTRACE'
  end if
!
  !if ((allocated(cludata).EQV..true.).AND.(isfirstcall.EQV..true.)) then
  if (isfirstcall.EQV..true.) then
    if (mod(nfeats,3).ne.0) then
      write(*,*) "CAMPARI FATAL. mod(nfeats,3).ne.0. This is a bug afaik."
      call fexit()                     
    end if
    if (clstsz.ne.nfeats/3) then 
      write(*,*) "CAMPARI FATAL. clstsz.ne.nfeats/3. This is a bug afaik."
      call fexit()                     
    end if
    if (size(pigros_cdofset,dim=1).ne.clstsz) then
      write(*,*) "CAMPARI FATAL. size(pigros_cdofset,dim=1).ne.clstsz. This is a bug afaik."
      call fexit()                     
    end if
    if (allocated(cludata).EQV..false.) then
      write(*,*) "CAMPARI FATAL. cludata not allocated at first call. This is a readcfile bug afaik."
      call fexit()                     
    end if
    write(pigros_ofileu,*) "Allocating cludata"
    deallocate(cludata)
    allocate(cludata(calcsz,nsnaps*nrepls)) !theoretically should work for any cdistance as I enforce calling cfile
    cludata = 1.0                           !useful for weights
    myrank = 0                              !this routine is for the moment called only by the GROMACS supermaster
    mpi_nodes = re_conditions               !it is the same in original campari
    isfirstcall = .false.
    if (allocated(pigros_nmap).EQV..true.) deallocate(pigros_nmap)
    allocate(pigros_nmap(mpi_nodes))
  end if
  select case (cdis_crit)
    case (1)
      write(*,*) "CAMPARI FATAL. CDISTANCE 1 is to be developed yet."
      call fexit()
    case (3)
      write(*,*) "CAMPARI FATAL. CDISTANCE 3 is to be developed yet."
      call fexit()
    case (5)
      write(*,*) "CAMPARI FATAL. CDISTANCE 5 is to be developed yet."
      call fexit()
    case (7)
      if (clstsz.ne.calcsz) call fexit()                !to be removed mfd
      if (size(pigros_cdofset,dim=2).ne.2) call fexit() !to be removed mfd
      do j=1,nsnaps*nrepls
        write(pigros_ofileu,*) 'column (snap*rep): ',j
        iii = 1 
        do i=1,clstsz
          ii = 1 + ( pigros_catmmap(i,1) - 1 )*3 !assuming we are in 3D. This is an index.
          jj = 1 + ( pigros_catmmap(i,2) - 1 )*3
          write(pigros_ofileu,*) 'row (feature), atoms: ',i,pigros_catms(pigros_catmmap(i,1)),&
 &                                                          pigros_catms(pigros_catmmap(i,2))
          if (pigros_catms(pigros_catmmap(i,1)).ne.pigros_cdofset(i,1).OR.&
 &           (pigros_catms(pigros_catmmap(i,2)).ne.pigros_cdofset(i,2))) then !mfd
            write(*,*) "CAMPARI FATAL. Wrong map (1)."
            call fexit()
          end if
          do k=1,3
            coor1(k) = pigs_cludata_campa(ii+k-1,j)
            coor2(k) = pigs_cludata_campa(jj+k-1,j)
          end do
          write(pigros_ofileu,*) 'coor1(1:3): ',coor1(1),coor1(2),coor1(3)
          write(pigros_ofileu,*) 'coor2(1:3): ',coor2(1),coor2(2),coor2(3)
          featval = (coor1(1)-coor2(1))**2 + (coor1(2)-coor2(2))**2 + (coor1(3)-coor2(3))**2
          if (cdistransform.eq.3) then
            tmpzt = 0.5*PI/cdistrans_params(1)
          end if
          if (cdistransform.eq.0) then
            cludata(iii,j) = sqrt(featval)
          else if (cdistransform.eq.1) then
            cludata(iii,j) = 1.0 - 1.0/(1.0 + exp(-(sqrt(featval)-cdistrans_params(1))/cdistrans_params(2)))
          else if (cdistransform.eq.2) then
            cludata(iii,j) = (sqrt(featval)+cdistrans_params(1))**(-1.0/cdistrans_params(2))
          else if (cdistransform.eq.3) then
            featval = sqrt(featval)
            if (featval.le.cdistrans_params(1)) then
              cludata(iii,j) = sin(featval*tmpzt)
            else
              cludata(iii,j) = 1.0
            end if
          end if
          write(pigros_ofileu,*) 'cludata(iii,j): ',cludata(iii,j)
          iii = iii + 1 !no weights, no jumps in cludata
        end do
      end do
    case default
      write(*,*) "CAMPARI FATAL. Got an unexpected CDISTANCE. This is a bug afaik."
      call fexit()
  end select
!
  call pigros_MPI_ASMaster(istep,tpi)
!
  close(iretr)
  write(pigros_ofileu,*) 'Done with pigros_docludata'
  write(pigros_ofileu,*) "------------------------------------"
  flush(pigros_ofileu)
  write(*,*) 'Done with pigros_docludata'
!
end subroutine pigros_docludata
!
!-------------------------------------------------------------------------
!
!
subroutine pigros_MPI_ASMaster(istep,tpi)
!
  use iounit
  use atoms
  use accept
  use molecule
!  use mpi
  use mpistuff
  use mcsums
  use energies
  use torsn
  use system
  use movesets
  use forces
  use sequen
  use ems
  use cutoffs
  use zmatrix
  use clusters
  use fyoc
  use grandensembles
  use interfaces
#ifdef ENABLE_THREADS
  use threads
#endif
!
  implicit none
!
  integer, INTENT(IN):: tpi
  integer(KIND=8), INTENT(IN):: istep
!
  integer tpn,azero,ixl,cpibu,medi(mpi_nodes,8)
  integer i,j,masterrank,k
  integer nmap(2*mpi_nodes)
  RTYPE random
  logical afalse,sayyes
  integer, ALLOCATABLE:: progind(:),invvec(:),iv2(:)
  integer(KIND=8) ee1,ee2
  real(KIND=4), ALLOCATABLE:: distv(:)
  real(KIND=4) ddv(mpi_nodes,2)
#ifdef ENABLE_THREADS
  integer OMP_GET_NUM_THREADS
  logical OMP_IN_PARALLEL
!
  if (tpi.le.0) then
    if (OMP_IN_PARALLEL().EQV..true.) then
      write(pigros_ofileu,*) 'Fatal. When using hybrid MPI/multi-threaded code, MPI_ASMaster(...) must always be called &
 &by all threads of the binding region (instead got a thread identifier of zero). This is a bug.'
      call fexit()
    end if
  end if
#endif
!
#ifdef ENABLE_THREADS
!$OMP MASTER
#endif
  write(*,*) "Start of pigros_MPI_ASMaster"
  write(pigros_ofileu,*) "------------------------------------"
  write(pigros_ofileu,*) "Start of pigros_MPI_ASMaster"
#ifdef ENABLE_THREADS
!$OMP END MASTER
!$OMP BARRIER
#endif
!
  masterrank = 0
  azero = 0
  afalse = .false.
  sayyes = .true.
!
  if (myrank.eq.masterrank) then
#ifdef ENABLE_THREADS
    if (tpi.gt.0) then
      tpn = OMP_GET_NUM_THREADS()
    else
      tpn = 1
    end if
#else
    tpn = 1
#endif
!   analyze data
    if (pdb_analyze.EQV..false.) then
      write(*,*) "CAMPARI FATAL. Got pdb_analyze.EQV..false.. This is a bug afaik."
      call fexit()
    end if
    ixl = cstored/mpi_nodes
#ifdef ENABLE_THREADS
!$OMP BARRIER
!$OMP SINGLE
#endif
!   fix some parameters if necessary
    if (csmoothie.ge.(cstored/2-1)) then
      write(pigros_ofileu,*) 'Warning. Selected smoothing window size is too large (FMCSC_CSMOOTHORDER ',csmoothie,').'
      csmoothie = cstored/2-1
    end if
#ifdef ENABLE_THREADS
!$OMP END SINGLE
#endif
!
!   cluster new data
    if (cstored.gt.0) then
      if (tpi.le.1) call System_Clock(ee1)
      k = -2
#ifdef ENABLE_THREADS
!$OMP BARRIER
!$OMP MASTER
#endif
      if (cprepmode.gt.0) call preprocess_cludata(azero)
      if (cstored.gt.4) call repopulate_weights()
#ifdef ENABLE_THREADS
!$OMP END MASTER
!$OMP BARRIER
#endif
#ifdef ENABLE_THREADS
        call birch_clustering_threads(k,nstruccls,tpi)
!$OMP BARRIER
#else
        call birch_clustering(k,nstruccls)
#endif
#ifdef ENABLE_THREADS
!$OMP BARRIER
!$OMP MASTER
#endif
#ifdef ENABLE_THREADS
!$OMP END MASTER
!$OMP BARRIER
#endif
!     get approximate MST
      call gen_MST_from_treeclustering(tpi)
#ifdef ENABLE_THREADS
!$OMP BARRIER
!$OMP MASTER
#endif
      cpibu = cprogindstart
      allocate(distv(cstored))
      allocate(progind(cstored))
      allocate(invvec(cstored+2))
      allocate(iv2(cstored))
      cprogindstart = birchtree(c_nhier+1)%cls(1)%center ! center of largest cluster in hierarchical tree
!     generate progress index
      call gen_progind_from_adjlst(approxmst,cprogindstart,progind,distv,invvec,iv2)
      cprogindstart = cpibu
!     sort replicas' final snapshots by PI position, by PI distance from nearest other final ss, and by associated SST edge length
!     all sorts in decreasing order; maps are yielded in medi(:,5:7) with associated ranks per replica
!     from the ranks, get a flipped composite rank and recover map in medi(:,8)
      do i=ixl,cstored,ixl
        medi(i/ixl,1) = invvec(i+1)
        medi(i/ixl,2) = i/ixl
!        write(pigros_ofileu,*) 'Final pos ',i/ixl,medi(i/ixl,1) 
      end do
      i = 1
      j = mpi_nodes
      call merge_sort(ldim=mpi_nodes,up=afalse,list=medi(:,1),olist=medi(:,3),ilo=i,ihi=j,idxmap=medi(:,2),olist2=medi(:,5))
      do i=1,mpi_nodes
        medi(medi(i,5),2) = i
      end do
      medi(:,5) = medi(:,2)
      do i=1,mpi_nodes
        if (medi(i,5).eq.1) then
          medi(i,1) = invvec(i*ixl+1) - medi(medi(i,5)+1,3)
        else if (medi(i,5).eq.mpi_nodes) then
          medi(i,1) = medi(medi(i,5)-1,3) - invvec(i*ixl+1)
        else
         medi(i,1) = min(invvec(i*ixl+1)-medi(medi(i,5)+1,3),medi(medi(i,5)-1,3)-invvec(i*ixl+1))
        end if
        medi(i,2) = i
!        write(pigros_ofileu,*) 'Diff from ',i,medi(i,1)
      end do
      i = 1
      j = mpi_nodes
      call merge_sort(ldim=mpi_nodes,up=afalse,list=medi(:,1),olist=medi(:,3),ilo=i,ihi=j,idxmap=medi(:,2),olist2=medi(:,6))
      do i=1,mpi_nodes
        medi(medi(i,6),2) = i
      end do
      medi(:,6) = medi(:,2)
      do i=ixl,cstored,ixl
        ddv(i/ixl,1) = distv(invvec(i+1))
        medi(i/ixl,2) = i/ixl
        !write(pigros_ofileu,*) 'Length ',i/ixl,ddv(i/ixl,1)
      end do
      i = 1
      j = mpi_nodes
      call merge_sort(ldim=mpi_nodes,up=afalse,list=ddv(:,1),olist=ddv(:,2),ilo=i,ihi=j,idxmap=medi(:,2),olist2=medi(:,7))
      do i=1,mpi_nodes
        medi(medi(i,7),2) = i
      end do
      medi(:,7) = medi(:,2)
      do i=1,mpi_nodes
        medi(i,1) = sum(mpi_nodes+1-medi(i,5:7))
        medi(i,2) = i
      end do
      i = 1
      j = mpi_nodes
      call merge_sort(ldim=mpi_nodes,up=afalse,list=medi(:,1),olist=medi(:,3),ilo=i,ihi=j,idxmap=medi(:,2),olist2=medi(:,8))
!
!     now use the computed parameters to determine whether to reseed each replica
!     note that a replica being reseeded means that its own conformation is lost (no swaps allowed)
      do i=1,mpi_nodes
        nmap(i) = i-1
      end do
      do j=re_aux(7)+1,mpi_nodes
        i = medi(j,8)
        k = min(re_aux(7),ceiling(random()*re_aux(7)))
        !write(pigros_ofileu,*) 'proposing ',i,' to ',k
        if (random().lt.(1.0*(medi(k,3)-medi(j,3))/(1.0*(medi(1,3)-medi(mpi_nodes,3)+1)))) then
          nmap(i) = medi(k,8)-1
          !write(pigros_ofileu,*) 'conditional accept'
        end if
      end do
!
!     generate distribution parameters for positions of replica trajectories in PI
      medi(:,1:4) = 0
      do i=1,cstored
        j = (progind(i)+ixl-1)/ixl
        medi(j,1) = medi(j,1) + 1
        if (medi(j,1).le.(ixl/4))   medi(j,3) = i
        if (medi(j,1).le.(ixl/2))   medi(j,2) = i
        if (medi(j,1).le.3*(ixl/4)) medi(j,4) = i
      end do
!      write(pigros_ofileu,*) '1/4IAN: ',medi(:,3)
!      write(pigros_ofileu,*) 'MEDIAN: ',medi(:,2)
!      write(pigros_ofileu,*) '3/4IAN: ',medi(:,4)
!      write(pigros_ofileu,*) medi(:,4)-medi(:,3)
!
!     uniqueness overwrite
      do i=1,mpi_nodes
        if ((medi(i,4)-medi(i,3)).lt.ixl) nmap(i) = i-1
      end do
!
      deallocate(iv2)
      deallocate(invvec)
      deallocate(progind)
      deallocate(distv)
#ifdef ENABLE_THREADS
!$OMP END MASTER
!$OMP BARRIER
#endif
      if (allocated(approxmst).EQV..true.) then
        do i=max(tpi,1),size(approxmst),tpn
          if (allocated(approxmst(i)%adj).EQV..true.) deallocate(approxmst(i)%adj)
          if (allocated(approxmst(i)%dist).EQV..true.) deallocate(approxmst(i)%dist)
        end do
#ifdef ENABLE_THREADS
!$OMP BARRIER
#endif
      end if
      if (allocated(birchtree).EQV..true.) then
        do i=1,c_nhier+1
          do j=max(tpi,1),birchtree(i)%ncls,tpn
            if (allocated(birchtree(i)%cls(j)%snaps).EQV..true.) deallocate(birchtree(i)%cls(j)%snaps)
            if (allocated(birchtree(i)%cls(j)%tmpsnaps).EQV..true.) deallocate(birchtree(i)%cls(j)%tmpsnaps)
            if (allocated(birchtree(i)%cls(j)%sums).EQV..true.) deallocate(birchtree(i)%cls(j)%sums)
            if (allocated(birchtree(i)%cls(j)%children).EQV..true.) deallocate(birchtree(i)%cls(j)%children)
          end do
        end do
#ifdef ENABLE_THREADS
!$OMP BARRIER
#endif
        do i=max(tpi,1),c_nhier+1,tpn
          deallocate(birchtree(i)%cls)
        end do
#ifdef ENABLE_THREADS
!$OMP BARRIER
#endif
      end if
      if (tpi.le.1) then
        deallocate(approxmst)
        deallocate(birchtree)
        call System_Clock(ee2)
        time_analysis = time_analysis + 1.0*(ee2 - ee1)
      end if
    else
      do i=1,mpi_nodes
        nmap(i) = i-1
      end do
    end if
!
!   print trace so breaks can be removed
    if (inst_retr.EQV..true.) then
#ifdef ENABLE_THREADS
!$OMP MASTER
#endif
      nmap(1:mpi_nodes) = nmap(1:mpi_nodes) + 1
      call pigros_MPI_REPrtTrace(istep,nmap(:))
      nmap(1:mpi_nodes) = nmap(1:mpi_nodes) - 1
      pigros_nmap(1:mpi_nodes) = nmap(1:mpi_nodes)
#ifdef ENABLE_THREADS
!$OMP END MASTER
!$OMP BARRIER
#endif
    end if
!
  end if !if (myrank==masterrank)
#ifdef ENABLE_THREADS
!$OMP MASTER
#endif
  write(pigros_ofileu,*) "Done with pigros_MPI_ASMaster"
  write(pigros_ofileu,*) "------------------------------------"
  flush(pigros_ofileu)
  write(*,*) "Done with pigros_MPI_ASMaster"
#ifdef ENABLE_THREADS
!$OMP END MASTER
!$OMP BARRIER
#endif

!
end subroutine pigros_MPI_ASMaster
!
!---------------------------------------------------------------------------
! 
!
!---------------------------------------------------------------------
!
subroutine pigros_MPI_REPrtTrace(istep,prtmap)
!
  use clusters, ONLY: pigros_ofileu
  use mcsums
  use mpistuff
!
  implicit none
!
  integer(KIND=8), INTENT(IN):: istep
!
  integer bk,bk2,azero,prtmap(mpi_nodes)
  character(max(1,ceiling(log10(1.0*mpi_nodes+0.5)))) bnrs
  character(max(1,ceiling(log10(1.0*ceiling(log10(1.0*mpi_nodes+0.5)))))) bnrs2
  character(MAXSTRLEN) fmtstr
!
  write(*,*) "Start of pigros_MPI_REPrtTrace"
  write(pigros_ofileu,*) "------------------------------------"
  write(pigros_ofileu,*) "Start pigros_MPI_REPrtTrace"
!
  azero = 0
!
  bk = max(1,ceiling(log10(1.0*mpi_nodes+0.5)))
  call int2str(mpi_nodes,bnrs,azero)
  bk2 = max(1,ceiling(log10(1.0*ceiling(log10(1.0*mpi_nodes+0.5)))))
  azero = 0
  call int2str(bk,bnrs2,azero)
  fmtstr='(i12,1x,'//bnrs(1:bk)//'(i'//bnrs2(1:bk2)//',1x))'
! write(pigros_ofileu,fmtstr) istep,prtmap
  write(iretr,fmtstr) istep,prtmap
!
  write(pigros_ofileu,*) "Done with pigros_MPI_REPrtTrace"
  write(pigros_ofileu,*) "------------------------------------"
  flush(pigros_ofileu)
  write(*,*) "Done with pigros_MPI_REPrtTrace"
!
end subroutine pigros_MPI_REPrtTrace
!
!---------------------------------------------------------------------
!
